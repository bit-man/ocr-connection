import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/presentation/trello_destination_widget_controller.dart';

import '../../trello_widget_controller_test.dart';
import '../../utils/change_notifier_tools.dart';

late TrelloDestinationWidgetController controller;
late TestTrelloRepository trello;
late PreferencesRepository preferences;

const buttonText = 'button text';
Board board = Board('1', 'Board 1');
Lists list = Lists('1', 'List 1');
const noList = null;
const noBoard = null;

main() {
  setUp(() {
    trello = TestTrelloRepository();
    preferences = TestPreferences();
    controller = TrelloDestinationWidgetController(buttonText, trello, preferences, []);
  });

  test('create text', () {
    Text buttonPressedText = controller.buttonPressedText();

    expect(buttonPressedText.data, buttonText);
  });

  test('disable button when neither board no list are selected', () async {
    await notification(() => controller.board = noBoard, controller);
    await notification(() => controller.list = noList, controller);

    var isDisabled = controller.isButtonDisabled;

    expect(isDisabled, isTrue);
  });

  test('disable button when list is null', () async {
    await notification(() => controller.board = board, controller);
    await notification(() => controller.list = noList, controller);

    var isDisabled = controller.isButtonDisabled;

    expect(isDisabled, isTrue);
  });

  test('disable button when board is null', () async {
    await notification(() => controller.board = noBoard, controller);
    await notification(() => controller.list = list, controller);

    var isDisabled = controller.isButtonDisabled;

    expect(isDisabled, isTrue);
  });

  test('return callback button board and list have been selected', () async {
    await notification(() => controller.board = board, controller);
    await notification(() => controller.list = list, controller);

    var isDisabled = controller.isButtonDisabled;

    expect(isDisabled, isFalse);
  });

  test('create card when board and list are set', () async {
    await notification(() => controller.board = board, controller);
    await notification(() => controller.list = list, controller);

    await controller.createCard();

    expect(trello.cardid, isNotNull);
  });
}
