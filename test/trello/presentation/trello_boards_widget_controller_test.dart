import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/presentation/trello_boards_widget_controller.dart';

import '../../trello_widget_controller_test.dart';
import '../../utils/change_notifier_tools.dart';

const firstBoardName = 'first board name';
Board firstBoard = Board("1", firstBoardName);
Board secondBoard = Board("2", 'second board name');

late PreferencesRepository preferences;
late TrelloRepository trello;
late TrelloBoardsWidgetController controller;
late Board? currentBoard;

main() {
  void onChanged(Board? value) => currentBoard = value;

  setUp(() {
    preferences = TestPreferences();
    trello = TestTrelloRepository(board: [firstBoard, secondBoard]);
    controller = TrelloBoardsWidgetController(trello, preferences, onChanged);
    currentBoard = null;
  });

  test('provide no board name on start', () {
    var currentBoardName = controller.currentBoard?.name;

    expect(currentBoardName, isNull);
  });

  test('change current board name to changed board', () async {
    await notification(() => controller.boards(), controller);
    controller.onBoardNameChange(firstBoardName);
    expect(controller.currentBoard, firstBoard);
  });

  test('change current board name to changed board even if it\'s null', () {
    controller.onBoardNameChange(null);
    expect(controller.currentBoard, null);
  });

  test(
      'change current board name to changed board, notifies on completion and executes'
      ' callback', () async {
    await notification(() => controller.boards(), controller);
    await notification(() => controller.onBoardNameChange(firstBoardName), controller);

    expect(controller.currentBoard, firstBoard);
    expect(currentBoard, firstBoard);
  });

  test('provide empty board names on start', () {
    var boardNames = controller.boardList;

    expect(boardNames, isEmpty);
  });

  test('retrieve boards and notifies on completion', () async {
    await notification(() => controller.boards(), controller);

    expect(controller.boardList, [firstBoard, secondBoard]);
  });
}
