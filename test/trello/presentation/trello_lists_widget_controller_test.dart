import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/presentation/trello_lists_widget_controller.dart';

import '../../trello_widget_controller_test.dart';
import '../../utils/change_notifier_tools.dart';

late PreferencesRepository preferences;
late TrelloRepository trello;
late TrelloListsWidgetController controller;

const firstBoardName = 'first board name';
Board firstBoard = Board("1", firstBoardName);
Board secondBoard = Board("2", 'second board name');
const firstListName = "List 1";
Lists firstList = Lists("l1", firstListName);
Lists secondList = Lists("l2", "List 2");
Lists? changedList;

main() {
  setUp(() {
    preferences = TestPreferences();
    trello = TestTrelloRepository(
        board: [firstBoard, secondBoard], listItems: [firstList, secondList]);
    controller = TrelloListsWidgetController(preferences, trello);
    changedList = null;
  });

  test('current list name is null on start', () {
    var listName = controller.currentList;

    expect(listName, isNull);
  });

  test('empty list names when no board name', () {
    var listNames = controller.listItems;

    expect(listNames, isEmpty);
  });

  test('empty list names, and notify, when retrieve list names and no board name',
      () async {
    await notification(() => controller.lists(), controller);

    expect(controller.listItems, isEmpty);
  });

  test('list names, and notify, when board name is set', () async {
    await notification(() => controller.lists(), controller);
    await notification(() => controller.board = firstBoard, controller);

    expect(controller.listItems, [firstList, secondList]);
  });

  test('maintain a valid current list name when board name is changed', () async {
    await notification(() => controller.board = firstBoard, controller);

    expect(controller.currentList, isNull);
  });

  test('null current list name when no list name change', () {
    var currentListNames = controller.currentList;

    expect(currentListNames, isNull);
  });

  test('current list name is set and notify and execute callback, on list name change',
      () async {
    controller.listChangeCallback = onListChange;
    await notification(() => controller.board = firstBoard, controller);
    await notification(() => controller.onListNameChange(firstListName), controller);

    expect(controller.currentList, firstList);
    expect(changedList, firstList);
  });

  test('not notify when no callback', () async {
    await notification(() => controller.board = firstBoard, controller);
    await notification(() => controller.onListNameChange(firstListName), controller);

    expect(changedList, isNull);
  });

  test('current list name is null, and notify, on list name change to null', () async {
    controller.listChangeCallback = onListChange;
    await notification(() => controller.onListNameChange(null), controller);

    expect(controller.currentList, isNull);
    expect(changedList, isNull);
  });
}

void onListChange(Lists? value) => changedList = value;
