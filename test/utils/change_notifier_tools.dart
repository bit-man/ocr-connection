import 'package:flutter/cupertino.dart';
import 'package:mockito/mockito.dart';

class DummyCallback extends Mock {
  call();
}

notification(Function() f, ChangeNotifier changeNotifier) async {
  var callback = DummyCallback();
  changeNotifier.addListener(callback);
  dynamic name = f();
  await untilCalled(callback());
  return name;
}

awaitCompletion(Function() f, ChangeNotifier changeNotifier) async =>
    notification(f, changeNotifier);
