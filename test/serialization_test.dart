import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';

main() {
  test("simple deserialize", () {
    String jsonString = '{"name":"aaa"}';
    Map<String, dynamic> attribute = jsonDecode(jsonString);
    expect(attribute['name'], 'aaa');
  });

  test("simple array deserialize", () {
    String jsonString = '[{"name":"aaa"},{"name":"bbb"}]';
    var attribute = jsonDecode(jsonString);
    expect(attribute[0]['name'], 'aaa');
    expect(attribute[1]['name'], 'bbb');
  });

  test("complex array deserialize", () {
    String jsonString = '[{"id": 1,"name":"aaa"},{"id": 2,"name":"bbb"}]';
    var attribute = jsonDecode(jsonString);
    expect(attribute[0]['id'], 1);
    expect(attribute[0]['name'], 'aaa');
    expect(attribute[1]['id'], 2);
    expect(attribute[1]['name'], 'bbb');
  });
}
