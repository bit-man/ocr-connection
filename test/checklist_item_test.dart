import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/checklist_item.dart';

main() {
  test('id deserialization', () {
    String jsonString = '{"idChecklist": "1"}';
    var decode = jsonDecode(jsonString);
    var checklistItem = ChecklistItem.fromJson(decode);
    expect(checklistItem.id, "1");
  });

  test('card deserialization with no id throws exception', () {
    String jsonString = '{}';
    var decode = jsonDecode(jsonString);
    expect(() => ChecklistItem.fromJson(decode), throwsA(isA<Exception>()));
  });
}
