import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/infrastructure/uri_builder.dart';

const key = 'mykey';
const token = 'mytoken';
const listId = 'list-id';

late UriBuilder builder;

main() {
  setUp(() {
    builder = UriBuilder(key);
  });

  test('uri boards creation', () {
    Uri boardsUri = builder.boards(token);
    expect(boardsUri.toString(),
        'https://api.trello.com/1/members/me/boards?key=mykey&token=mytoken');
  });

  test('uri lists creation', () {
    Uri listsUri = builder.lists(token, listId);
    expect(listsUri.toString(),
        'https://api.trello.com/1/boards/list-id/lists?key=mykey&token=mytoken');
  });

  test('uri card creation', () {
    Uri cardUri = builder.card(token, 'listId');
    expect(
        cardUri.toString(),
        'https://api.trello.com/1/cards?idList=listId&'
        'key=mykey&token=mytoken&name=ToDo&position=top');
  });

  test('uri checklist creation', () {
    Uri checklistUri = builder.checklist(token, 'cardId');
    expect(checklistUri.toString(),
        'https://api.trello.com/1/checklists?idCard=cardId&key=mykey&token=mytoken');
  });

  test('uri checklist item creation', () {
    Uri checklistUri = builder.checklistItem(token, 'checklistId', 'comprar batata');
    expect(
        checklistUri.toString(),
        'https://api.trello.com/1/checklists/checklistId/checkItems?name=comprar+batata'
        '&key=mykey&token=mytoken');
  });
}
