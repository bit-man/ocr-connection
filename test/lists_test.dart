import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/lists.dart';

main() {
  test('board deserialization', () {
    String jsonString = '{"id": "1","name":"aaa"}';
    var listsJson = jsonDecode(jsonString);
    var list = Lists.fromJson(listsJson);
    expect(list.name, 'aaa');
    expect(list.id, "1");
  });

  test('board deserialization with no name throws exception', () {
    String jsonString = '{"id": 1}';
    var listJson = jsonDecode(jsonString);
    expect(() => Lists.fromJson(listJson), throwsA(isA<Exception>()));
  });
}
