import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/board.dart';

main() {
  test('board deserialization', () {
    String jsonString = '{"id": "1","name":"aaa"}';
    var boardJson = jsonDecode(jsonString);
    var board = Board.fromJson(boardJson);
    expect(board.name, 'aaa');
    expect(board.id, "1");
  });

  test('board deserialization with no name throws exception', () {
    String jsonString = '{"id": 1}';
    var boardJson = jsonDecode(jsonString);
    expect(() => Board.fromJson(boardJson), throwsA(isA<Exception>()));
  });
}
