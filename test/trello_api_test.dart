import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/infrastructure/uri_builder.dart';

const trelloApiKey = '3f78b538b78737bd4b95cc05589030c7';
const trelloToken = String.fromEnvironment('TRELLO_TOKEN');
const idDoesntCare = 'dont-care-id';
const nameDoesntCare = 'dont-care-name';
Board boardDoesntCare = Board(idDoesntCare, nameDoesntCare);
Lists listDoesntCare = Lists(idDoesntCare, nameDoesntCare);
Lists listTesting = Lists('623eeddfe3944c5131b7a48f', 'Testing OCR Connection');

Board bitmanPersonalBoard = Board('502e6bfabd3229aa2756b12b', "Bit-Man Personal");

main() {
  test('calling with invalid key throws exception', () async {
    var uriBuilder = UriBuilder('invalid key');
    var api = TrelloApi(uriBuilder);

    expect(() async => await api.boards('invalid token'), throwsA(isA<Exception>()));
  });

  test('calling with valid key returns boards', () async {
    var uriBuilder = UriBuilder(trelloApiKey);
    var api = TrelloApi(uriBuilder);

    var boards = await api.boards(trelloToken);
    expect(boards.length, 9);
  });

  test('calling with invalid key throws exception', () async {
    var uriBuilder = UriBuilder('invalid key');
    var api = TrelloApi(uriBuilder);

    expect(() {
      return api.lists('invalid token', boardDoesntCare);
    }, throwsA(isA<Exception>()));
  });

  test('calling with invalid key throws exception', () async {
    var uriBuilder = UriBuilder('invalid key');
    var api = TrelloApi(uriBuilder);

    expect(() {
      return api
          .card('invalid token', listDoesntCare, ['comprar batata', 'pedir crédito']);
    }, throwsA(isA<Exception>()));
  });

  test('calling with invalid key throws exception', () async {
    var uriBuilder = UriBuilder(trelloApiKey);
    var api = TrelloApi(uriBuilder);

    var card =
        await api.card(trelloToken, listTesting, ['comprar batata', 'pedir crédito']);

    expect(card.id, isNotNull);
  });

  test('calling with valid key returns lists', () async {
    var uriBuilder = UriBuilder(trelloApiKey);
    var api = TrelloApi(uriBuilder);

    var lists = await api.lists(trelloToken, bitmanPersonalBoard);
    expect(lists.length, 11);
  });
}
