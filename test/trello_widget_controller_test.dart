import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/card.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/presentation/trello_token_widget_controller.dart';

const myToken = 'my Token';

main() {
  late TestPreferences preferences;
  late TestTrelloRepository trelloApi;
  late TrelloTokenWidgetController controller;
  late Function() expectedChanges;

  setUp(() {
    preferences = TestPreferences();
    trelloApi = TestTrelloRepository();
  });

  tearDown(() {
    controller.removeListener(expectedChanges);
  });

  bool tokenSavedAndAuthorized() =>
      preferences.token == myToken &&
      preferences.hasBeenCalledSaveTrelloToken &&
      controller.isTrelloAuthorized &&
      trelloApi.hasBeenBoardsCalled &&
      preferences.hasBeenCalledTrelloToken;

  bool tokenSaveCallFailsAndNotAuthorized() =>
      preferences.hasBeenCalledSaveTrelloToken &&
      !controller.isTrelloAuthorized &&
      trelloApi.hasBeenBoardsCalled &&
      preferences.hasBeenCalledTrelloToken;

  void thenVerify(Function isDone) {
    expectedChanges = expectAsyncUntil0(() => {}, () {
      return isDone.call();
    });
    controller.addListener(expectedChanges);
  }

  givenTrelloTokenStored(bool isStored) {
    preferences.isTrelloTokenStored = isStored;
    controller = TrelloTokenWidgetController(preferences, trelloApi);
  }

  tokenNotSavedAndNotAuthorized() =>
      preferences.hasBeenCalledTrelloToken && !controller.isTrelloAuthorized;

  tokenSavedAndAuthorizedInConstructor() =>
      preferences.hasBeenCalledTrelloToken && controller.isTrelloAuthorized;

  test('should save token when authorized', () {
    givenTrelloTokenStored(true);

    thenVerify(tokenSavedAndAuthorized);

    controller.saveTrelloToken(myToken);
  });

  test('should not authorize when token save fails', () {
    preferences.saveTrelloTokenResponse = false;
    givenTrelloTokenStored(true);

    thenVerify(tokenSaveCallFailsAndNotAuthorized);

    controller.saveTrelloToken(myToken);
  });

  test('should not authorize when token not stored', () {
    givenTrelloTokenStored(false);

    thenVerify(tokenNotSavedAndNotAuthorized);
  });

  test('should authorize when token stored', () {
    givenTrelloTokenStored(true);

    thenVerify(tokenSavedAndAuthorizedInConstructor);
  });
}

class TestTrelloRepository implements TrelloRepository {
  var _boardsCalls = 0;

  final List<Board> board;

  final List<Lists> listItems;

  late String _cardId;

  TestTrelloRepository({this.board = const [], this.listItems = const []});

  bool get hasBeenBoardsCalled => _boardsCalls > 0;

  get cardid => _cardId;

  @override
  Future<List<Board>> boards(String token) {
    _boardsCalls++;
    return Future(() => board);
  }

  @override
  Future<List<Lists>> lists(String token, Board board) {
    return Future(() => listItems);
  }

  @override
  Future<Card> card(String token, Lists list, List<String> item) {
    _cardId = '1';
    return Future(() => Card(_cardId));
  }
}

class TestPreferences implements Preferences {
  var token = 'uninitialized token';

  var _trelloTokenCalls = 0;

  var _saveTokenCalls = 0;

  var saveTrelloTokenResponse = true;

  get hasBeenCalledTrelloToken => _trelloTokenCalls > 0;

  bool isTrelloTokenStored = true;

  bool get hasBeenCalledSaveTrelloToken => _saveTokenCalls > 0;

  @override
  Future<bool> saveTrelloToken(String token) {
    this.token = token;
    _saveTokenCalls++;
    return Future(() => saveTrelloTokenResponse);
  }

  @override
  String trelloApiKey() {
    // TODO: implement trelloApiKey
    throw UnimplementedError();
  }

  @override
  String trelloAuthorizationUrl() {
    // TODO: implement trelloAuthorizationUrl
    throw UnimplementedError();
  }

  @override
  Future<String?> trelloToken() {
    _trelloTokenCalls++;
    if (isTrelloTokenStored) return Future(() => myToken);
    return Future(() => null);
  }
}
