import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/infrastructure/board_response.dart';

main() {
  test('get board names', () {
    String jsonString = '['
        '{"id": "1","name":"aaa"},'
        '{"id": "2","name":"bbb"}'
        ']';
    var boardJson = jsonDecode(jsonString);
    var response = BoardResponse.fromJson(boardJson);
    expect(response.boards, [Board("1", "aaa"), Board("2", "bbb")]);
  });

  test('no board name throws exception', () {
    String jsonString = '['
        '{"id": "1"}'
        ']';
    var boardJson = jsonDecode(jsonString);
    expect(() => BoardResponse.fromJson(boardJson), throwsA(isA<Exception>()));
  });

  test('no boards returns no names', () {
    String jsonString = '[]';
    var boardJson = jsonDecode(jsonString);
    var response = BoardResponse.fromJson(boardJson);
    expect(response.boards, []);
  });
}
