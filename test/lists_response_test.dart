import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/lists_response.dart';

main() {
  test('get board names', () {
    String jsonString = '['
        '{"id": "1","name":"aaa"},'
        '{"id": "2","name":"bbb"}'
        ']';
    var listsJson = jsonDecode(jsonString);
    var response = ListsResponse.fromJson(listsJson);
    expect(response.lists, [Lists("1", "aaa"), Lists("2", "bbb")]);
  });

  test('no board name throws exception', () {
    String jsonString = '['
        '{"id": "1"}'
        ']';
    var listsJson = jsonDecode(jsonString);
    expect(() => ListsResponse.fromJson(listsJson), throwsA(isA<Exception>()));
  });

  test('no boards returns no names', () {
    String jsonString = '[]';
    var listsJson = jsonDecode(jsonString);
    var response = ListsResponse.fromJson(listsJson);
    expect(response.lists, []);
  });
}
