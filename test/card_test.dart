import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:ocr_connection/trello/domain/card.dart';

main() {
  test('id deserialization', () {
    String jsonString = '{"id": "1"}';
    var decode = jsonDecode(jsonString);
    var card = Card.fromJson(decode);
    expect(card.id, "1");
  });

  test('card deserialization with no id throws exception', () {
    String jsonString = '{}';
    var decode = jsonDecode(jsonString);
    expect(() => Card.fromJson(decode), throwsA(isA<Exception>()));
  });
}
