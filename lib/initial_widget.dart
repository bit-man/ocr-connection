import 'package:flutter/material.dart';
import 'package:ocr_connection/trello/presentation/trello_token_widget_controller.dart';
import 'package:ocr_connection/widget_factory.dart';
import 'package:provider/provider.dart';

class InitialWidget extends StatelessWidget {
  const InitialWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Provider.of<TrelloTokenWidgetController>(context);

    if (controller.isTrelloAuthorized) {
      return WidgetFactory.Main();
    }
    return WidgetFactory.TrelloToken();
  }
}
