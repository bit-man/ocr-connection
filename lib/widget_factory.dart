// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:ocr_connection/ocr/ocr_service.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/infrastructure/uri_builder.dart';
import 'package:ocr_connection/trello/presentation/trello_boards_widget.dart';
import 'package:ocr_connection/trello/presentation/trello_boards_widget_controller.dart';
import 'package:ocr_connection/trello/presentation/trello_destination_widget.dart';
import 'package:ocr_connection/trello/presentation/trello_destination_widget_controller.dart';
import 'package:ocr_connection/trello/presentation/trello_lists_widget.dart';
import 'package:ocr_connection/trello/presentation/trello_lists_widget_controller.dart';
import 'package:provider/provider.dart';

import 'initial_widget.dart';
import 'ocr/infrastructure/preferences.dart';
import 'ocr/presentation/main_widget.dart';
import 'trello/presentation/trello_token_widget.dart';

const appTitle = 'OCR Connection';
const createCardText = 'Create card';

typedef BoardCallback = void Function(Board? value);
typedef ListsCallback = void Function(Lists? value);

class WidgetFactory {
  static Widget Initial() => const InitialWidget();

  static Widget Main() => _scaffold_decorate(
      ChangeNotifierProvider(create: (_) => OCRService(), child: const MainWidget()));

  static Widget TrelloToken() => _scaffold_decorate(const TrelloTokenWidget());

  static Widget _scaffold_decorate(Widget widget) {
    return Scaffold(
        backgroundColor: Colors.white70,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Text(appTitle),
        ),
        body: widget);
  }

  static Widget TrelloDestination(List<String> item) => ChangeNotifierProvider(
      create: (_) {
        return TrelloDestinationWidgetController(
            createCardText, _trelloApi(), _preferences(), item);
      },
      child: const TrelloDestinationWidget());

  static TrelloApi _trelloApi() {
    var key = _preferences().trelloApiKey();
    return TrelloApi(UriBuilder(key));
  }

  static PreferencesRepository _preferences() => Preferences();

  static Widget TrelloBoards(BoardCallback onChanged) => ChangeNotifierProvider(
      create: (_) {
        return TrelloBoardsWidgetController(_trelloApi(), _preferences(), onChanged);
      },
      child: const TrelloBoardsWidget());

  static TrelloListsWidgetController TrelloListsController() =>
      TrelloListsWidgetController(_preferences(), _trelloApi());

  static Widget TrelloLists(TrelloListsWidgetController controller) =>
      ChangeNotifierProvider.value(value: controller, child: const TrelloListsWidget());
}
