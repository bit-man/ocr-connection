class ChecklistItem {
  late String _id;

  ChecklistItem(this._id);

  ChecklistItem.fromJson(Map<String, dynamic> attribute) {
    var __id = attribute['idChecklist'];
    if (__id == null) throw Exception("Checklist item id not provided");
    _id = attribute['idChecklist'];
  }

  get id => _id;

  @override
  bool operator ==(Object other) {
    return other is ChecklistItem && other._id == _id;
  }
}
