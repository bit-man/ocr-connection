class Checklist {
  late String _id;

  Checklist(this._id);

  Checklist.fromJson(Map<String, dynamic> attribute) {
    var __id = attribute['id'];
    if (__id == null) throw Exception("Checklist id not provided");
    _id = attribute['id'];
  }

  get id => _id;

  @override
  bool operator ==(Object other) {
    return other is Checklist && other._id == _id;
  }
}
