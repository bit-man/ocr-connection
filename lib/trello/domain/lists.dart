class Lists {
  late String _name;

  late String _id;

  Lists(this._id, this._name);

  Lists.fromJson(Map<String, dynamic> attribute) {
    var __name = attribute['name'];
    if (__name == null) throw Exception("List name not provided");
    _name = __name;
    _id = attribute['id'];
  }

  get name => _name;

  get id => _id;

  @override
  bool operator ==(Object other) {
    return other is Lists && other._name == _name && other._id == _id;
  }
}
