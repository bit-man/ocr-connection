class Board {
  late String _name;

  late String _id;

  Board(this._id, this._name);

  Board.fromJson(Map<String, dynamic> attribute) {
    var __name = attribute['name'];
    if (__name == null) throw Exception("Board name not provided");
    _name = __name;
    _id = attribute['id'];
  }

  get name => _name;

  get id => _id;

  @override
  bool operator ==(Object other) {
    return other is Board && other._name == _name && other._id == _id;
  }
}
