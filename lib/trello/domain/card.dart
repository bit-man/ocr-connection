class Card {
  late String _id;

  Card(this._id);

  Card.fromJson(Map<String, dynamic> attribute) {
    var __id = attribute['id'];
    if (__id == null) throw Exception("List name not provided");
    _id = attribute['id'];
  }

  get id => _id;

  @override
  bool operator ==(Object other) {
    return other is Card && other._id == _id;
  }
}
