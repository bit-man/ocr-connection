import 'package:flutter/material.dart';
import 'package:ocr_connection/widget_factory.dart';
import 'package:provider/provider.dart';

import 'trello_token_widget_controller.dart';

const tokenLabel = 'Please grant Trello access\nPaste token here';

class TrelloTokenWidget extends StatelessWidget {
  const TrelloTokenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TrelloTokenWidgetController>(
        builder: (context, controller, _) => buildWidget(context, controller));
  }

  buildWidget(BuildContext context, TrelloTokenWidgetController controller) {
    controller.requestTrelloAuthorization();
    return Center(
        child: TextField(
            style: const TextStyle(fontSize: 25),
            obscureText: true,
            textAlign: TextAlign.left,
            textAlignVertical: TextAlignVertical.center,
            decoration: const InputDecoration(
                label: Center(
                  child: Text(
                    tokenLabel,
                    textAlign: TextAlign.center,
                  ),
                ),
                border: OutlineInputBorder()),
            onSubmitted: (token) => {
                  controller
                      .saveTrelloToken(token)
                      .then((_) => navigateBackToInitial(context))
                }));
  }

  navigateBackToInitial(BuildContext context) {
    return Navigator.push(context,
        MaterialPageRoute(builder: (context) => WidgetFactory.Initial()));
  }
}
