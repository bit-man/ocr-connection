import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../infrastructure/api.dart';

class TrelloTokenWidgetController with ChangeNotifier {
  bool __trelloAuthorizationNeeded = false;

  final PreferencesRepository _preferences;

  final TrelloRepository _trelloApi;

  TrelloTokenWidgetController(this._preferences, this._trelloApi) {
    _initTrelloAuthorizationNeeded();
  }

  bool get isTrelloAuthorized => !__trelloAuthorizationNeeded;

  void _initTrelloAuthorizationNeeded() => _preferences
      .trelloToken()
      .then((token) => _tokenRetrieved(token), onError: _tokenRetrieveError);

  _tokenRetrieveError(e) => _setAuthorizationNeeded(true);

  void _tokenRetrieved(String? trelloToken) {
    if (trelloToken == null) {
      _setAuthorizationNeeded(true);
      return;
    }

    _trelloApi.boards(trelloToken).then(_noTrelloAuthorizationNeeded,
        onError: _trelloAuthorizationNeeded);
  }

  FutureOr<bool> _noTrelloAuthorizationNeeded(_) =>
      _setAuthorizationNeeded(false);

  _trelloAuthorizationNeeded(e) => _setAuthorizationNeeded(true);

  bool _setAuthorizationNeeded(bool value) {
    __trelloAuthorizationNeeded = value;
    notifyListeners();
    return value;
  }

  Future saveTrelloToken(String token) {
    return _preferences
        .saveTrelloToken(token)
        .then(_tokenSaved, onError: _tokenSaveFailed);
  }

  void _tokenSaved(bool saved) => _setAuthorizationNeeded(!saved);

  void _tokenSaveFailed(Object e) => _setAuthorizationNeeded(false);

  void requestTrelloAuthorization() {
    // ToDo notify if can't launch browser
    launch(_preferences.trelloAuthorizationUrl());
  }
}
