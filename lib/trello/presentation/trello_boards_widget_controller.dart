import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';

class TrelloBoardsWidgetController with ChangeNotifier {
  final TrelloRepository _trello;

  final PreferencesRepository _preferences;

  final void Function(Board? value) _onChanged;

  List<Board> _boardList = [];

  Board? _currentBoard;

  TrelloBoardsWidgetController(this._trello, this._preferences, this._onChanged);

  Board? get currentBoard => _currentBoard;

  List<Board> get boardList => _boardList;

  onBoardNameChange(String? boardName) {
    var boards = _boardList.where((board) => board.name == boardName);
    _currentBoard = boards.isEmpty ? null : boards.first;
    notifyListeners();
    _onChanged.call(_currentBoard);
  }

  boards() {
    _preferences.trelloToken().then(_retrieveBoards);
  }

  FutureOr<List<String>> _retrieveBoards(token) =>
      // Token here has been validated at app init then can't be neither null not invalid
      _trello.boards(token!).then(_saveBoards);

  FutureOr<List<String>> _saveBoards(List<Board> boards) {
    _boardList = boards;
    notifyListeners();
    return Future(() => List.empty());
  }
}
