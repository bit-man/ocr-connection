import 'package:flutter/material.dart';
import 'package:ocr_connection/trello/presentation/list_string_extension.dart';
import 'package:ocr_connection/trello/presentation/trello_boards_widget_controller.dart';
import 'package:provider/provider.dart';

class TrelloBoardsWidget extends StatelessWidget {
  const TrelloBoardsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Provider.of<TrelloBoardsWidgetController>(context);
    controller.boards();
    return DropdownButton<String>(
        value: controller.currentBoard?.name,
        items: controller.boardList.toDropdownItems(),
        onChanged: (boardName) => controller.onBoardNameChange(boardName));
  }
}
