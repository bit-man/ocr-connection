import 'package:flutter/material.dart';
import 'package:ocr_connection/trello/presentation/trello_destination_widget_controller.dart';
import 'package:ocr_connection/trello/presentation/trello_lists_widget_controller.dart';
import 'package:provider/provider.dart';

import '../../widget_factory.dart';
import '../domain/board.dart';
import '../domain/lists.dart';

class TrelloDestinationWidget extends StatefulWidget {
  const TrelloDestinationWidget({Key? key}) : super(key: key);

  @override
  State<TrelloDestinationWidget> createState() => _TrelloDestinationWidgetState();
}

class _TrelloDestinationWidgetState extends State<TrelloDestinationWidget> {
  late TrelloDestinationWidgetController _controller;

  final TrelloListsWidgetController _trelloListsWidgetController =
      WidgetFactory.TrelloListsController();

  @override
  Widget build(BuildContext context) {
    _controller = Provider.of<TrelloDestinationWidgetController>(context);
    _trelloListsWidgetController.listChangeCallback = onListChange;

    return Column(
      children: [
        WidgetFactory.TrelloBoards(onBoardChange),
        WidgetFactory.TrelloLists(_trelloListsWidgetController),
        ElevatedButton(
            onPressed: _controller.isButtonDisabled ? null : _controller.createCard,
            child: _controller.buttonPressedText())
      ],
    );
  }

  void onBoardChange(Board? value) {
    _trelloListsWidgetController.board = value;
    _controller.board = value;
  }

  void onListChange(Lists? value) => _controller.list = value;
}
