import 'package:flutter/material.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';

import '../domain/board.dart';
import '../domain/lists.dart';

class TrelloDestinationWidgetController with ChangeNotifier {
  final String _createCardText;

  Lists? _list;

  Board? _board;

  final TrelloRepository _trello;

  final PreferencesRepository _preferences;

  final List<String> _item;

  TrelloDestinationWidgetController(
      this._createCardText, this._trello, this._preferences, this._item);

  set list(Lists? list) {
    _list = list;
    notifyListeners();
  }

  set board(Board? board) {
    _board = board;
    notifyListeners();
  }

  bool get isButtonDisabled => _neitherBoardNorListSelected();

  bool _neitherBoardNorListSelected() => _board == null || _list == null;

  Text buttonPressedText() => Text(_createCardText);

  Future<void> createCard() async {
    var token = await _preferences.trelloToken();
    await _trello.card(token!, _list!, _item);
  }
}
