import 'package:flutter/material.dart';
import 'package:ocr_connection/trello/presentation/list_string_extension.dart';
import 'package:ocr_connection/trello/presentation/trello_lists_widget_controller.dart';
import 'package:provider/provider.dart';

class TrelloListsWidget extends StatelessWidget {
  const TrelloListsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Provider.of<TrelloListsWidgetController>(context);
    controller.lists();
    return DropdownButton<String>(
        value: controller.currentList?.name,
        items: controller.listItems.toDropdownItems(),
        onChanged: (listName) => controller.onListNameChange(listName));
  }
}
