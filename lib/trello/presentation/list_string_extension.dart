import 'package:flutter/material.dart';
import 'package:ocr_connection/trello/domain/board.dart';

import '../domain/lists.dart';

extension ListBoardExtension on List<Board> {
  List<DropdownMenuItem<String>> toDropdownItems() => map<DropdownMenuItem<String>>(
          (board) => DropdownMenuItem<String>(value: board.name, child: Text(board.name)))
      .toList();
}

extension ListListsExtension on List<Lists> {
  List<DropdownMenuItem<String>> toDropdownItems() => map<DropdownMenuItem<String>>(
          (list) => DropdownMenuItem<String>(value: list.name, child: Text(list.name)))
      .toList();
}
