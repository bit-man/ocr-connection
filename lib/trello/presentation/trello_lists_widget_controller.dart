import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/widget_factory.dart';

class TrelloListsWidgetController with ChangeNotifier {
  final PreferencesRepository _preferences;
  final TrelloRepository _trello;

  TrelloListsWidgetController(this._preferences, this._trello);

  Board? _board;
  List<Lists> _lists = [];

  Lists? _currentList;

  Lists? get currentList => _currentList;

  ListsCallback? _callback;

  set listChangeCallback(ListsCallback callback) => _callback = callback;

  set board(Board? value) {
    _board = value;
    _currentList = null;
    lists();
  }

  List<Lists> get listItems => _lists;

  onListNameChange(String? listName) {
    var firstWhere = _lists.where((list) => list.name == listName);
    _currentList = firstWhere.isEmpty ? null : firstWhere.first;
    notifyListeners();
    _executeCallback();
  }

  void _executeCallback() {
    if (_hasCallback()) _callback!.call(_currentList);
  }

  void lists() {
    _preferences.trelloToken().then(_retrieveLists);
  }

  FutureOr<List<Lists>> _retrieveLists(String? token) {
    if (_isDefinedBoardName()) {
      return _trello.lists(token!, _board!).then(_saveLists);
    }
    return _saveLists(List.empty());
  }

  bool _isDefinedBoardName() => _board != null;

  FutureOr<List<Lists>> _saveLists(List<Lists> lists) {
    _lists = lists;
    notifyListeners();
    return Future(() => List<Lists>.empty());
  }

  bool _hasCallback() => _callback != null;
}
