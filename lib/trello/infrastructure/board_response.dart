import '../domain/board.dart';

class BoardResponse {
  final List<Board> _boards = List.empty(growable: true);

  BoardResponse.fromJson(List<dynamic> boards) {
    boards.forEach((board) {
      _boards.add(Board.fromJson(board));
    });
  }

  List<Board> get boards => _boards;
}
