class UriBuilder {
  static const String boardsPath = '1/members/me/boards';
  static const String listsPath = '1/boards/{id}/lists';
  static const String cardPath = '1/cards';
  static const String checklistPath = '1/checklists';
  static const String checklistItemPath = '1/checklists/{id}/checkItems';

  static const String endpoint = 'api.trello.com';

  String key;

  UriBuilder(this.key);

  Uri boards(String token) =>
      Uri.https(endpoint, boardsPath, {'key': key, 'token': token});

  Uri lists(String token, String listId) => Uri.https(
      endpoint, listsPath.replaceAll("{id}", listId), {'key': key, 'token': token});

  Uri card(String token, String listId) => Uri.https(endpoint, cardPath,
      {'idList': listId, 'key': key, 'token': token, 'name': 'ToDo', 'position': 'top'});

  Uri checklist(String token, String cardId) =>
      Uri.https(endpoint, checklistPath, {'idCard': cardId, 'key': key, 'token': token});

  Uri checklistItem(String token, String checklistId, String content) => Uri.https(
      endpoint,
      checklistItemPath.replaceAll("{id}", checklistId),
      {'name': content, 'key': key, 'token': token});
}
