import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ocr_connection/trello/domain/board.dart';
import 'package:ocr_connection/trello/domain/checklist.dart';
import 'package:ocr_connection/trello/domain/lists.dart';
import 'package:ocr_connection/trello/infrastructure/lists_response.dart';
import 'package:ocr_connection/trello/infrastructure/uri_builder.dart';

import '../domain/card.dart';
import 'board_response.dart';

class TrelloApi implements TrelloRepository {
  UriBuilder uriBuilder;

  TrelloApi(this.uriBuilder);

  @override
  Future<List<Board>> boards(String token) async {
    var uri = uriBuilder.boards(token);
    var jsonResponse = await _get(uri);
    return BoardResponse.fromJson(jsonResponse).boards;
  }

  @override
  Future<List<Lists>> lists(String token, Board board) async {
    var uri = uriBuilder.lists(token, board.id);
    var jsonResponse = await _get(uri);
    return ListsResponse.fromJson(jsonResponse).lists;
  }

  Future<dynamic> _get(Uri uri) async {
    http.Response response = await http.get(uri, headers: {'Accept': 'application/json'});
    return jsonDecode(response.body);
  }

  @override
  Future<Card> card(String token, Lists list, List<String> item) async {
    var card = await _createCard(token, list);
    var checklist = await _createChecklist(token, card);
    _createChecklistItems(item, token, checklist);
    return card;
  }

  void _createChecklistItems(List<String> item, String token, Checklist checklist) =>
      item.forEach((checklistItem) => _createItem(token, checklist, checklistItem));

  void _createItem(String token, Checklist checklist, String checklistItem) {
    var checklistUri = uriBuilder.checklistItem(token, checklist.id, checklistItem);
    _put(checklistUri);
  }

  Future<Checklist> _createChecklist(String token, Card card) async {
    var checklistUri = uriBuilder.checklist(token, card.id);
    var jsonResponse = await _put(checklistUri);
    return Checklist.fromJson(jsonResponse);
  }

  Future<Card> _createCard(String token, Lists list) async {
    var cardUri = uriBuilder.card(token, list.id);
    var jsonResponse = await _put(cardUri);
    return Card.fromJson(jsonResponse);
  }

  Future<dynamic> _put(Uri uri) async {
    var response = await http.post(uri, headers: {'Accept': 'application/json'});
    return jsonDecode(response.body);
  }
}

abstract class TrelloRepository {
  Future<List<Board>> boards(String token);

  Future<List<Lists>> lists(String token, Board board);

  Future<Card> card(String token, Lists list, List<String> item);
}
