import '../domain/lists.dart';

class ListsResponse {
  final List<Lists> _lists = List.empty(growable: true);

  ListsResponse.fromJson(List<dynamic> lists) {
    lists.forEach((list) {
      _lists.add(Lists.fromJson(list));
    });
  }

  get boardNames => _lists.map<String>((board) => board.name).toList();

  List<Lists> get lists => _lists;
}
