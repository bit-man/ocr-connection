import 'package:flutter_mobile_vision_2/flutter_mobile_vision_2.dart';

class CameraRepository {
  final int _ocrCamera = FlutterMobileVision.CAMERA_BACK;

  Future<List<String>> read() async {
    var ocrTexts = await FlutterMobileVision.read(
        camera: _ocrCamera, waitTap: true, multiple: true);
    return ocrTexts.map((e) => e.value).toList();
  }
}
