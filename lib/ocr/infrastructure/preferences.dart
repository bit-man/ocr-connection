import 'package:shared_preferences/shared_preferences.dart';

class Preferences implements PreferencesRepository {
  static const trelloTokenKey = 'trello_token';
  static const trelloAuthUrl =
      'https://trello.com/1/authorize?expiration=1day&scope=read,write,account&response_type=token&name=OCR%20Connection&key=';

  @override
  Future<String?> trelloToken() => _get(trelloTokenKey);

  Future<String?> _get(String key) async {
    var preferences = await SharedPreferences.getInstance();
    return preferences.getString(key);
  }

  @override
  String trelloApiKey() => '3f78b538b78737bd4b95cc05589030c7';

  @override
  Future<bool> saveTrelloToken(String token) => _save(trelloTokenKey, token);

  Future<bool> _save(String key, String value) async {
    var preferences = await SharedPreferences.getInstance();
    return preferences.setString(key, value);
  }

  @override
  String trelloAuthorizationUrl() => trelloAuthUrl + trelloApiKey();
}

abstract class PreferencesRepository {
  Future<String?> trelloToken();

  String trelloApiKey();

  Future<bool> saveTrelloToken(String token);

  String trelloAuthorizationUrl();
}
