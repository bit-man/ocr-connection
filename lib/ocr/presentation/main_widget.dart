import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widget_factory.dart';
import '../ocr_service.dart';

const scanButtonText = 'Scan';

class MainWidget extends StatelessWidget {
  const MainWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<OCRService>(
      builder: (context, ocr, _) => buildMain(ocr, context.read()),
    );
  }

  Widget buildMain(OCRService ocr, OCRService ocrExec) {
    var items = List<Widget>.empty(growable: true);
    items.addAll(scannedTextFrom(ocr));
    items.addAll([scanButton(ocrExec), WidgetFactory.TrelloDestination(ocr.text)]);
    return ListView(children: items);
  }

  Widget scanButton(OCRService ocrExec) {
    return ElevatedButton(
      onPressed: () => ocrExec.scan(),
      child: const Text(scanButtonText),
    );
  }

  List<Widget> scannedTextFrom(OCRService ocr) => ocr.text.map((e) => Text(e)).toList();
}
