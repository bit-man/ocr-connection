import 'package:flutter/foundation.dart';

import 'infrastructure/camera_repository.dart';

class OCRService with ChangeNotifier {
  final CameraRepository _repository = CameraRepository();
  static const String scannedTextInitial = "Scanned text will be shown here";
  static const String textRecognitionFailed = 'Failed to recognize text';

  // ToDo is ok to access this way? Guidelines?
  List<String> text = [scannedTextInitial];

  void scan() {
    _repository.read().then(_saveScannedText).catchError(_onError);
  }

  void _saveScannedText(List<String> value) {
    text = value;
    notifyListeners();
  }

  void _onError(Object error) {
    text = [textRecognitionFailed, error.toString()];
    notifyListeners();
  }
}
