import 'package:flutter/material.dart';
import 'package:ocr_connection/app_widget.dart';
import 'package:ocr_connection/ocr/infrastructure/preferences.dart';
import 'package:ocr_connection/trello/infrastructure/api.dart';
import 'package:ocr_connection/trello/infrastructure/uri_builder.dart';
import 'package:ocr_connection/trello/presentation/trello_token_widget_controller.dart';
import 'package:provider/provider.dart';

void main() {
  var preferences = Preferences();
  var uriBuilder = UriBuilder(preferences.trelloApiKey());
  TrelloRepository trelloApi = TrelloApi(uriBuilder);
  runApp(ChangeNotifierProvider(
    create: (_) => TrelloTokenWidgetController(preferences, trelloApi),
    child: const AppWidget(),
  ));
}
