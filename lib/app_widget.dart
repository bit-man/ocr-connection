import 'package:flutter/material.dart';
import 'package:ocr_connection/widget_factory.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: SafeArea(
      child: WidgetFactory.Initial(),
    ));
  }
}
