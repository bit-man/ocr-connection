#!/usr/bin/env bash

thisFolder=$(dirname $0)

[[ -z $1 ]] && echo "ERROR: Missing commit message" && exit -1

perl -i -pe 's/^(version:\s+\d+\.\d+\.\d+\+)(\d+)$/$1.($2+1)/e' ${thisFolder}/../pubspec.yaml
git add -A
git commit -m "$@"